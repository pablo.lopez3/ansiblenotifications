# Slack Notifications with Ansible

## Roles

This repository contains the following ansible roles:

### send-pipeline-notifications

This role uses the 'slack' ansible module to send a message that notifies a pipeline build has succeeded or failed. The message includes the following custom data:
		
- Repository name
- Branch in which the build was run
- Pipeline URL
- Status of the build
- Job on which the notification was calles
		
All this information needs to be set as environment variables, as well as the token needed by the SlackAPI:

#### Variable
- status

#### Environment
- PROJECT_ID
- REPO
- BRANCH
- PIPELINE
- TOKEN
- JOB
- SUCCESSFUL_CHANNEL (Channel to send notification if status is true)
- FAILED_CHANNEL (Channel to send notification if status is false)
